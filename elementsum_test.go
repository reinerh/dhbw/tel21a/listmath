package listmath

import "fmt"

func Example_ElementSum() {
	l1 := []int{2, 1, 17, 25, 3, 12, 42, 5}
	l2 := []int{21, 3, 2, 15, 4, 2, 25, 12}
	l3 := []int{21, 3, 2, 15}

	fmt.Println(ElementSum(l1, l2))
	fmt.Println(ElementSum(l1, l1))
	fmt.Println(ElementSum(l2, l2))

	fmt.Println(ElementSum(l1, l3))
	fmt.Println(ElementSum(l3, l1))

	// Output:
	// [23 4 19 40 7 14 67 17]
	// [4 2 34 50 6 24 84 10]
	// [42 6 4 30 8 4 50 24]
	// [23 4 19 40 3 12 42 5]
	// [23 4 19 40 3 12 42 5]
}

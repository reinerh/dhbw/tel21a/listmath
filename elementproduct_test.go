package listmath

import "fmt"

func Example_ElementProduct() {
	l1 := []int{2, 1, 17, 25, 3, 12, 42, 5}
	l2 := []int{21, 3, 2, 15, 4, 2, 25, 12}

	fmt.Println(ElementProduct(l1, l2))

	// Output:
	// [42 3 34 375 12 24 1050 60]

}

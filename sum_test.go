package listmath

import "fmt"

func Example_Sum() {
	l1 := []int{2, 1, 17, 25, 3, 12, 42, 5}
	l2 := []int{2}
	l3 := []int{1, 2, 3, 4}
	l4 := []int{5, 4, 3, 2, 1}

	fmt.Println(Sum(l1))
	fmt.Println(Sum(l2))
	fmt.Println(Sum(l3))
	fmt.Println(Sum(l4))

	// Output:
	// 107
	// 2
	// 10
	// 15
}

package listmath

// Erwartet zwei Listen l1 und l2.
// Liefert eine neue Liste, die an Stelle i die Summe l1[i] + l2[i] enthält.
func ElementSum(l1, l2 []int) []int {
	result := make([]int, 0)

	// Wichtig: l1 muss die kürzere Liste sein.
	//          Dafür sorgen wir erstmal:
	if len(l1) > len(l2) {
		l1, l2 = l2, l1
	}

	// Erstmal den gemeinsamen Teil von l1 und l2 abarbeiten.
	for i := 0; i < len(l1); i++ {
		result = append(result, l1[i]+l2[i])
	}

	// Die restlichen Elemente aus l2 anhängen.
	for i := len(l1); i < len(l2); i++ {
		result = append(result, l2[i])
	}

	return result
}

package listmath

// Erwartet eine Liste und liefert die Summe aller Elemente der Liste.
func Sum(l []int) int {
	result := 0
	for _, v := range l {
		result += v
	}
	return result
}
